package com.voyager.app.model;

public class RatingPlace {
    private Long idRatingPlace;
    private Long idPlace;
    private Long idUser;
    private Long countLikes;

    public Long getIdRatingPlace() {
        return idRatingPlace;
    }

    public void setIdRatingPlace(Long idRatingPlace) {
        this.idRatingPlace = idRatingPlace;
    }

    public Long getIdPlace() {
        return idPlace;
    }

    public void setIdPlace(Long idPlace) {
        this.idPlace = idPlace;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getCountLikes() {
        return countLikes;
    }

    public void setCountLikes(Long countLikes) {
        this.countLikes = countLikes;
    }
}
