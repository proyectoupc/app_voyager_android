package com.voyager.app.model;

public class SavePlace {
    private long idSavePlace;
    private long idUser;
    private long idPlace;
    private Place place;

    public long getIdSavePlace() {
        return idSavePlace;
    }

    public void setIdSavePlace(long idSavePlace) {
        this.idSavePlace = idSavePlace;
    }

    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    public long getIdPlace() {
        return idPlace;
    }

    public void setIdPlace(long idPlace) {
        this.idPlace = idPlace;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }
}
