package com.voyager.app.model;

import java.util.List;

public class Place {
    private Long idPlace;
    private String name;
    private String description;
    private String tags;
    private String postDateTime;
    private float latitude;
    private float longitude;
    private int rating;
    private int views;
    private Long idUser;
    private User user;
    private String contactName;
    private String contactPhone;
    private List<Integer> idPhotos;
    private Long idPlaceSave;

    public Long getIdPlace() {
        return idPlace;
    }

    public void setIdPlace(Long idPlace) {
        this.idPlace = idPlace;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getPostDateTime() {
        return postDateTime;
    }

    public void setPostDateTime(String postDateTime) {
        this.postDateTime = postDateTime;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public List<Integer> getIdPhotos() {
        return idPhotos;
    }

    public void setIdPhotos(List<Integer> idPhotos) {
        this.idPhotos = idPhotos;
    }

    public Long getIdPlaceSave() {
        return idPlaceSave;
    }

    public void setIdPlaceSave(Long idPlaceSave) {
        this.idPlaceSave = idPlaceSave;
    }
}
