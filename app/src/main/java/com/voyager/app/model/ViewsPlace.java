package com.voyager.app.model;

public class ViewsPlace {
    private Long idViewsPlace;
    private Long idPlace;
    private Long idUser;
    private Long countViews;

    public Long getIdViewsPlace() {
        return idViewsPlace;
    }

    public void setIdViewsPlace(Long idViewsPlace) {
        this.idViewsPlace = idViewsPlace;
    }

    public Long getIdPlace() {
        return idPlace;
    }

    public void setIdPlace(Long idPlace) {
        this.idPlace = idPlace;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getCountViews() {
        return countViews;
    }

    public void setCountViews(Long countViews) {
        this.countViews = countViews;
    }
}
