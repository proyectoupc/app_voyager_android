package com.voyager.app.model;

public class Follows {
    private Long idFollower;
    private Long idUser;
    private Long idUserFollower;
    private String dayFollowed;
    private String userName;
    private User user;

    public Long getIdFollower() {
        return idFollower;
    }

    public void setIdFollower(Long idFollower) {
        this.idFollower = idFollower;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getIdUserFollower() {
        return idUserFollower;
    }

    public void setIdUserFollower(Long idUserFollower) {
        this.idUserFollower = idUserFollower;
    }

    public String getDayFollowed() {
        return dayFollowed;
    }

    public void setDayFollowed(String dayFollowed) {
        this.dayFollowed = dayFollowed;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
