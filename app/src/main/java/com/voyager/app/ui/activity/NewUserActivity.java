package com.voyager.app.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.voyager.app.R;
import com.voyager.app.constant.Constant;
import com.voyager.app.data.UserDAO;
import com.voyager.app.data.sqliteUtil.DAOException;
import com.voyager.app.model.User;
import com.voyager.app.utils.OkHttpUtil;
import com.voyager.app.utils.UIMessages;

import java.io.IOException;

public class NewUserActivity extends AppCompatActivity {
    private EditText txtNombre, txtEmail, txtPassword, txtConfirmPassword;
    private UIMessages uiMessages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        uiMessages = new UIMessages(this);
        txtNombre = (EditText) findViewById(R.id.txtUserName);
        txtEmail = (EditText) findViewById(R.id.txtuserEmail);
        txtPassword = (EditText) findViewById(R.id.txtUserPassword);
        txtConfirmPassword = (EditText) findViewById(R.id.txtUserConfirmPassword);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu_register:
                try {
                    if (txtNombre.getText().toString().trim().equals("")) {
                        uiMessages.showToast("Ingrese el nombre", false);
                        txtNombre.requestFocus();
                    } else if (txtEmail.getText().toString().trim().equals("")) {
                        uiMessages.showToast("Ingrese el email", false);
                        txtEmail.requestFocus();
                    }
                    else if (!txtEmail.getText().toString().contains("@")){
                        uiMessages.showToast("Ingrese un email válido", false);
                        txtEmail.requestFocus();
                    }
                    else if (txtPassword.getText().toString().trim().equals("")) {
                        uiMessages.showToast("Ingrese el password", false);
                        txtPassword.requestFocus();
                    } else if (txtConfirmPassword.getText().toString().equals("")) {
                        uiMessages.showToast("Confirme el password", false);
                        txtConfirmPassword.requestFocus();
                    } else if (!txtPassword.getText().toString().equals(txtConfirmPassword.getText().toString())) {
                        uiMessages.showToast("El password y la confirmación de password no son iguales", false);
                        txtPassword.requestFocus();
                    } else {
                        User user = new User();
                        user.setEmail(txtEmail.getText().toString());
                        user.setName(txtNombre.getText().toString());
                        user.setPassword(txtPassword.getText().toString());
                        grabar(user);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void grabar(User user) {
        try {
            new AysncUser().execute(new Gson().toJson(user));
        } catch (android.os.NetworkOnMainThreadException e) {
            Log.e("TAG", "Error", e);
        }
    }

    private class AysncUser extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            uiMessages.showProgressDialog("Registrando...", "Espere por favor");
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.post(Constant.API_URL_USER + "/Register", strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            uiMessages.dismissProgressDialog();
            User userRergistered = new Gson().fromJson(s, User.class);
            if (userRergistered != null) {
                UserDAO dao = new UserDAO(getBaseContext());
                try {
                    dao.insertar(userRergistered);
                    txtNombre.setText("");
                    txtEmail.setText("");
                    txtPassword.setText("");
                    txtConfirmPassword.setText("");
                    Toast toast = Toast.makeText(getApplicationContext(), "Se registró correctamente", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();
                    startActivity(new Intent(NewUserActivity.this, MainActivity.class));
                    finish();
                } catch (DAOException e) {
                    Log.i("NewUserActi", "====> " + e.getMessage());
                }
            } else {
                uiMessages.showToast("Ocurrió un problema al intentar registrar, por favor vuelva a intentarlo", false);
            }

        }
    }
}
