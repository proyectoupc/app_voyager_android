package com.voyager.app.ui.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.voyager.app.R;
import com.voyager.app.constant.Constant;
import com.voyager.app.data.UserDAO;
import com.voyager.app.data.sqliteUtil.DAOException;
import com.voyager.app.model.Follows;
import com.voyager.app.model.User;
import com.voyager.app.ui.activity.AnotherProfileActivity;
import com.voyager.app.ui.fragment.ShowFollowersFragment;
import com.voyager.app.utils.OkHttpUtil;
import com.voyager.app.utils.UIMessages;

import java.io.IOException;
import java.util.List;

public class FollowsAdapter extends RecyclerView.Adapter<FollowsAdapter.ViewHolder> {
    private UIMessages uiMessages;
    private List<Follows> followsList;
    private Context context;

    private EventListener listener;

    public FollowsAdapter(List<Follows> followsList, Context context, EventListener listener) {
        this.followsList = followsList;
        this.context = context;
        this.listener = listener;
    }

    public interface EventListener {
        void onEvent(int data);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        uiMessages = new UIMessages(context);
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_follows, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Follows follows = followsList.get(position);
        holder.txtUserName.setText(follows.getUserName());
        holder.txtFollowDate.setText(follows.getDayFollowed());
        holder.imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uiMessages.showAlertDialog("ADVERTENCIA!!!", "¿Está seguro que desea eliminar?", "SI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Follows follows = followsList.get(holder.getAdapterPosition());
                        new AysncUser().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, follows.getIdFollower().toString());
                    }
                }, "NO", null, null, true);
            }
        });
        Picasso.get().load(Constant.API_URL_USER + "/GetPhoto/" + follows.getIdUserFollower())
                .error(R.drawable.no_user)
                .placeholder(R.drawable.loading)
                .into(holder.civUserPhoto, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("EROR", "ERROR", e);
                    }
                });
        holder.llFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = followsList.get(holder.getAdapterPosition()).getUser();
                context.startActivity(new Intent(context, AnotherProfileActivity.class).putExtra("userData", new Gson().toJson(user)));
            }
        });
    }


    @Override
    public int getItemCount() {
        return followsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtUserName;
        private TextView txtFollowDate;
        private com.mikhaellopez.circularimageview.CircularImageView civUserPhoto;
        private ImageView imgClose;
        private LinearLayout llFollow;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtUserName = itemView.findViewById(R.id.txtUserName);
            txtFollowDate = itemView.findViewById(R.id.txtFollowDate);
            imgClose = itemView.findViewById(R.id.imgClose);
            civUserPhoto = itemView.findViewById(R.id.civUserPhoto);
            llFollow = itemView.findViewById(R.id.llFollow);
        }
    }

    private class AysncUser extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            uiMessages.showProgressDialog("Eliminando...", "Espere por favor");
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.delete(Constant.API_URL_FOLLOWER + "/Delete/" + strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            uiMessages.dismissProgressDialog();
            listener.onEvent(0);
        }
    }
}
