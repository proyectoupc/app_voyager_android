package com.voyager.app.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.google.gson.Gson;
import com.voyager.app.R;
import com.voyager.app.constant.Constant;
import com.voyager.app.data.UserDAO;
import com.voyager.app.data.sqliteUtil.DAOException;
import com.voyager.app.model.User;
import com.voyager.app.utils.OkHttpUtil;
import com.voyager.app.utils.UIMessages;

import java.io.IOException;

public class RestorePasswordActivity extends AppCompatActivity {
    private UIMessages uiMessages;
    private EditText txtEmailRestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restore_password);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        uiMessages = new UIMessages(this);
        txtEmailRestore = findViewById(R.id.txtEmailRestore);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_restore_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu_restore_password:
                if (txtEmailRestore.getText().toString().trim().isEmpty()) {
                    uiMessages.showToast("Ingrese un correo", false);
                    txtEmailRestore.requestFocus();
                } else {
                    new AsyncRestorePassword().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, txtEmailRestore.getText().toString());
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class AsyncRestorePassword extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            uiMessages.showProgressDialog("Enviando solicitud de restauración", "Espere por favor...");
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.get(Constant.API_URL_USER + "/RestorePassword/" + strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            uiMessages.dismissProgressDialog();
            if (s != null) {
                if (s.equals("0")) {
                    uiMessages.showToast("No se reconoce el correo ingresado", true);
                } else {
                    uiMessages.showToast("Revise su correo para restablecer su contraseña", true);
                    startActivity(new Intent(RestorePasswordActivity.this, LoginActivity.class));
                    finish();
                }
            } else {
                uiMessages.showToast("Ocurrió un problema, intente más tarde", false);
            }
        }
    }
}
