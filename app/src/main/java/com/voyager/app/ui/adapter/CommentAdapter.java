package com.voyager.app.ui.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.voyager.app.R;
import com.voyager.app.constant.Constant;
import com.voyager.app.data.UserDAO;
import com.voyager.app.data.sqliteUtil.DAOException;
import com.voyager.app.model.Comment;
import com.voyager.app.model.Follows;
import com.voyager.app.model.User;
import com.voyager.app.ui.activity.AnotherProfileActivity;
import com.voyager.app.utils.OkHttpUtil;
import com.voyager.app.utils.UIMessages;

import java.io.IOException;
import java.util.List;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {
    private List<Comment> commentList;
    private Context context;
    private UIMessages uiMessages;
    private EventListener listener;
    private boolean isMyPlace = false;

    public CommentAdapter(List<Comment> commentList, Context context, EventListener listener, boolean isMyPlace) {
        this.commentList = commentList;
        this.context = context;
        uiMessages = new UIMessages(context);
        this.listener = listener;
        this.isMyPlace = isMyPlace;
    }

    public interface EventListener {
        void onEvent(int data);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_comment, parent, false);
        return new CommentAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Comment comment = commentList.get(position);
        holder.txtUserName.setText(comment.getUserName());
        holder.txtCommentDate.setText(comment.getDatetime());
        holder.lblComment.setText(comment.getDescription());
        Picasso.get().load(Constant.API_URL_USER + "/GetPhoto/" + comment.getIdUser())
                .error(R.drawable.no_user)
                .placeholder(R.drawable.cargar)
                .into(holder.civUserPhoto, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("ERROR", "ERROR", e);
                    }
                });
        UserDAO userDAO = new UserDAO(context);
        User user = null;
        try {
            user = userDAO.obtener();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        if ((user != null && user.getIdUser() == comment.getIdUser()) || isMyPlace) {
            holder.imgClose.setVisibility(ImageView.VISIBLE);
            holder.imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    uiMessages.showAlertDialog("ADVERTENCIA!!!", "¿Está seguro que desea eliminar?", "SI", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Comment comment1 = commentList.get(holder.getAdapterPosition());
                            new AsyncComment().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, comment1.getIdComentario() + "");
                        }
                    }, "NO", null, null, true);
                }
            });
        } else {
            holder.imgClose.setVisibility(ImageView.GONE);
        }
        holder.llComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Comment comment1 = commentList.get(holder.getAdapterPosition());
                context.startActivity(new Intent(context, AnotherProfileActivity.class).putExtra("userData", new Gson().toJson(comment1.getUser())));
            }
        });
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtUserName, txtCommentDate, lblComment;
        private com.mikhaellopez.circularimageview.CircularImageView civUserPhoto;
        private ImageView imgClose;
        private LinearLayout llComment;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtUserName = itemView.findViewById(R.id.txtUserName);
            txtCommentDate = itemView.findViewById(R.id.txtCommentDate);
            lblComment = itemView.findViewById(R.id.lblComment);
            civUserPhoto = itemView.findViewById(R.id.civUserPhoto);
            imgClose = itemView.findViewById(R.id.imgClose);
            llComment = itemView.findViewById(R.id.llComment);
        }

    }

    private class AsyncComment extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            uiMessages.showProgressDialog("Eliminando...", "Espere por favor");
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.delete(Constant.API_URL_COMMENT + "/Delete/" + strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            uiMessages.dismissProgressDialog();
            listener.onEvent(0);
        }
    }
}
