package com.voyager.app.ui.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.MarginLayoutParamsCompat;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.voyager.app.R;
import com.voyager.app.constant.Constant;
import com.voyager.app.data.UserDAO;
import com.voyager.app.data.sqliteUtil.DAOException;
import com.voyager.app.model.Follows;
import com.voyager.app.model.Photo;
import com.voyager.app.model.Place;
import com.voyager.app.model.User;
import com.voyager.app.service.SavePhotosService;
import com.voyager.app.ui.adapter.FollowsAdapter;
import com.voyager.app.utils.AppUtils;
import com.voyager.app.utils.OkHttpUtil;
import com.voyager.app.utils.SharedPreferencesUtils;
import com.voyager.app.utils.UIMessages;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NewPlaceActivity extends AppCompatActivity implements View.OnClickListener {
    private final static int REQUEST_CODE_CAMERA_PHOTO = 0;
    private final static int REQUEST_CODE_GALLERY_PHOTO = 1;
    private UIMessages uiMessages;
    private EditText txtTitle, txtDescription, txtTags, txtContactName, txtContactPhone;
    private ImageView imgPhoto;
    private String latitude, longitude;
    private LinearLayout llPhotoDinamic, llAddPhoto;
    private List<Photo> photoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_place);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        uiMessages = new UIMessages(this);
        txtTitle = findViewById(R.id.txtTitle);
        txtDescription = findViewById(R.id.txtDescription);
        txtTags = findViewById(R.id.txtTags);
        txtContactName = findViewById(R.id.txtContactName);
        txtContactPhone = findViewById(R.id.txtContactPhone);
        ImageButton ibtnCameraPhotos = findViewById(R.id.ibtnCameraPhotos);
        ImageButton ibtnGalleryPhotos = findViewById(R.id.ibtnGalleryPhotos);
        llPhotoDinamic = findViewById(R.id.llPhotoDinamic);
        llAddPhoto = findViewById(R.id.llAddPhoto);
        ibtnCameraPhotos.setOnClickListener(this);
        ibtnGalleryPhotos.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibtnCameraPhotos:
                int permissionCheck = ContextCompat.checkSelfPermission(
                        this, Manifest.permission.CAMERA);
                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                        requestPermission(Manifest.permission.CAMERA, 2);
                    } else {
                        requestPermission(Manifest.permission.CAMERA, 2);
                    }
                } else {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, REQUEST_CODE_CAMERA_PHOTO);
                }
                break;
            case R.id.ibtnGalleryPhotos:
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,
                        "Seleccione una foto"), REQUEST_CODE_GALLERY_PHOTO);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_new_publicacion, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_menu_publish:
                try {
                    String[] tags = txtTags.getText().toString().split(" ");
                    if (txtTitle.getText().toString().trim().equals("")) {
                        uiMessages.showToast("Ingrese el nombre del lugar.", true);
                        txtTitle.requestFocus();
                    } else if (txtDescription.getText().toString().trim().equals("")) {
                        uiMessages.showToast("Ingrese una descripción del lugar", true);
                        txtDescription.requestFocus();
                    } else if (txtTags.getText().toString().trim().equals("")) {
                        uiMessages.showToast("Ingrese 3 palabras clave para el lugar", true);
                        txtTags.requestFocus();
                    } else if (tags.length < 3) {
                        uiMessages.showToast("El número mínimo de tags es 3.", true);
                        txtTags.requestFocus();
                    } else if (latitude.isEmpty() || longitude.isEmpty()) {
                        uiMessages.showToast("Elija un lugar en el mapa", true);
                    } else if (llPhotoDinamic.getChildCount() == 0) {
                        uiMessages.showToast("Agregue al menos una foto", true);
                    } else {
                        UserDAO dao = new UserDAO(getBaseContext());
                        User user = dao.obtener();
                        Place place = new Place();
                        place.setName(txtTitle.getText().toString());
                        place.setDescription(txtDescription.getText().toString());
                        place.setTags(txtTags.getText().toString());
                        place.setContactName(txtContactName.getText().toString());
                        place.setContactPhone(txtContactPhone.getText().toString());
                        place.setIdUser(Long.valueOf(user.getIdUser() + ""));
                        String pattern = "dd/MM/yyyy HH:mm:ss";
                        DateFormat df = new SimpleDateFormat(pattern);
                        Date today = Calendar.getInstance().getTime();
                        String todayAsString = df.format(today);
                        place.setPostDateTime(todayAsString);
                        place.setLatitude(Float.valueOf(latitude));
                        place.setLongitude(Float.valueOf(longitude));
                        photoList = new ArrayList<>();
                        Photo photo;
                        for (int i = 0; i < llPhotoDinamic.getChildCount(); i++) {
                            photo = new Photo();
                            //photo.setIdPlace(place.getIdPlace());
                            photo.setPhoto(AppUtils.getImgBase64((ImageView) ((FrameLayout) llPhotoDinamic.getChildAt(i)).getChildAt(0)));
                            photoList.add(photo);
                        }
                        new AysncpPlace().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Gson().toJson(place));
                    }
                } catch (DAOException e) {
                    e.printStackTrace();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void requestPermission(String permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions(this,
                new String[]{permissionName}, permissionRequestCode);
    }

    public void showMap(View view) {
        int permissionCheck = ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, 1);
            } else {
                requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, 1);
            }
        } else {
            startActivityForResult(new Intent(NewPlaceActivity.this, MapActivity.class), 666);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivityForResult(new Intent(NewPlaceActivity.this, MapActivity.class), 666);
                }
                break;
            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, REQUEST_CODE_CAMERA_PHOTO);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 666:
                if (resultCode == Activity.RESULT_OK) {
                    latitude = data.getStringExtra("latitude");
                    longitude = data.getStringExtra("longitude");
                }
                break;
            case REQUEST_CODE_CAMERA_PHOTO:
                assert data != null;
                if (resultCode == RESULT_OK) {
                    imgPhoto = new ImageView(this);
                    Bitmap image = (Bitmap) data.getExtras().get("data");
                    imgPhoto.setImageBitmap(image);
                    addDinamicPhoto(imgPhoto);
                }
                break;
            case REQUEST_CODE_GALLERY_PHOTO:
                if (resultCode == RESULT_OK) {
                    assert data != null;
                    imgPhoto = new ImageView(this);
                    Uri selectedImage = data.getData();
                    imgPhoto.setImageURI(selectedImage);
                    addDinamicPhoto(imgPhoto);
                }
                break;
        }

    }

    private void addDinamicPhoto(ImageView imgPhoto) {
        FrameLayout frameLayout = new FrameLayout(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(llPhotoDinamic.getWidth() - 100, 600));
        lp.bottomMargin = 20;
        lp.rightMargin = 50;
        lp.leftMargin = 50;
        frameLayout.setLayoutParams(lp);
        lp = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(llPhotoDinamic.getWidth() - 100, 600));
        imgPhoto.setLayoutParams(lp);
        frameLayout.addView(imgPhoto);
        LinearLayout llClose = new LinearLayout(this);
        llClose.setGravity(Gravity.END);
        lp = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(llPhotoDinamic.getWidth() - 100, 100));
        llClose.setLayoutParams(lp);
        ImageView imgClose = new ImageView(this);
        imgClose.setImageResource(R.drawable.icon_close);
        lp = new LinearLayout.LayoutParams(new LinearLayout.LayoutParams(100, 100));
        imgClose.setLayoutParams(lp);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                uiMessages.showAlertDialog("ADVERTENCIA!!!", "¿Está seguro que desea eliminar?", "SI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        llPhotoDinamic.removeViewAt((llPhotoDinamic).indexOfChild((View) view.getParent().getParent()));
                        if (llPhotoDinamic.getChildCount() >= 10) {
                            llAddPhoto.setVisibility(LinearLayout.GONE);
                        } else {

                            llAddPhoto.setVisibility(LinearLayout.VISIBLE);
                        }
                    }
                }, "NO", null, null, true);
            }
        });
        llClose.addView(imgClose);
        frameLayout.addView(llClose);
        frameLayout.setBackgroundResource(R.drawable.border_line_1);
        llPhotoDinamic.addView(frameLayout);
        if (llPhotoDinamic.getChildCount() >= 10) {
            llAddPhoto.setVisibility(LinearLayout.GONE);
        } else {

            llAddPhoto.setVisibility(LinearLayout.VISIBLE);
        }
    }

    private class AysncpPlace extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            uiMessages.showProgressDialog("Registrando...", "Espere por favor");
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.post(Constant.API_URL_PLACE + "/Create", strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            uiMessages.dismissProgressDialog();
            if (s != null) {
                Place place = new Gson().fromJson(s, Place.class);
                for (int i = 0; i < photoList.size(); i++) {
                    photoList.get(i).setIdPlace(place.getIdPlace());
                }
                try {
                    SharedPreferencesUtils sharedPreferences = new SharedPreferencesUtils(NewPlaceActivity.this);
                    if (sharedPreferences.SaveDataPhotos(new Gson().toJson(photoList)))
                        startService(new Intent(NewPlaceActivity.this, SavePhotosService.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                uiMessages.showToast("Lugar publicado correctamente, sus fotos se están cargando, puede ver en la barra de notificación", true);
                finish();
            }
        }
    }
}
