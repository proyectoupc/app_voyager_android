package com.voyager.app.ui.fragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.voyager.app.R;
import com.voyager.app.constant.Constant;
import com.voyager.app.data.UserDAO;
import com.voyager.app.data.sqliteUtil.DAOException;
import com.voyager.app.model.User;
import com.voyager.app.ui.activity.MainActivity;
import com.voyager.app.ui.activity.MapActivity;
import com.voyager.app.ui.activity.NewPlaceActivity;
import com.voyager.app.utils.AppUtils;
import com.voyager.app.utils.OkHttpUtil;
import com.voyager.app.utils.UIMessages;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;;

public class EditProfileFragment extends Fragment implements View.OnClickListener {
    private TextView txtEmail;
    private EditText txtName, txtLastname, txtBirthday, txtPassword, txtConfirmPassword, txtDescription;
    private RadioButton rbtMale, rbtFemale;
    private Button btnSave;
    private ImageButton ibtnCameraPhoto, ibtnGallerPhoto, ibtnCameraBackground, ibtnGalleryBackground;
    private UIMessages uiMessages;
    private ImageView imgPhoto, imgBackground;
    private final static int REQUEST_CODE_CAMERA_PHOTO = 0;
    private final static int REQUEST_CODE_GALLERY_PHOTO = 1;
    private final static int REQUEST_CODE_CAMERA_BACKGROUND = 2;
    private final static int REQUEST_CODE_GALLERY_BACKGROUND = 3;
    private final Calendar myCalendar = Calendar.getInstance();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            uiMessages = new UIMessages(getActivity());
            txtEmail = getActivity().findViewById(R.id.txtEmail);
            txtName = getActivity().findViewById(R.id.txtName);
            txtLastname = getActivity().findViewById(R.id.txtUserLastname);
            txtBirthday = getActivity().findViewById(R.id.txtBirthday);
            txtPassword = getActivity().findViewById(R.id.txtUserPassword);
            txtConfirmPassword = getActivity().findViewById(R.id.txtUserConfirmPassword);
            txtDescription = (EditText) getActivity().findViewById(R.id.txtDescriptionMP);
            rbtFemale = getActivity().findViewById(R.id.rbtFemale);
            rbtMale = getActivity().findViewById(R.id.rbtMale);
            btnSave = getActivity().findViewById(R.id.btnSave);
            imgPhoto = getActivity().findViewById(R.id.imgPhoto);
            imgBackground = getActivity().findViewById(R.id.imgBackgroundEditProfile);
            ibtnCameraPhoto = getActivity().findViewById(R.id.ibtnCameraPhoto);
            ibtnGallerPhoto = getActivity().findViewById(R.id.ibtnGallerPhoto);
            ibtnCameraBackground = getActivity().findViewById(R.id.ibtnCameraBackground);
            ibtnGalleryBackground = getActivity().findViewById(R.id.ibtnGalleryBackground);
            btnSave.setOnClickListener(this);
            ibtnCameraPhoto.setOnClickListener(this);
            ibtnGallerPhoto.setOnClickListener(this);
            ibtnCameraBackground.setOnClickListener(this);
            ibtnGalleryBackground.setOnClickListener(this);
            UserDAO DAO = new UserDAO(getActivity());
            User user = DAO.obtener();
            txtEmail.setText(user.getEmail());
            txtName.setText(user.getName());
            txtLastname.setText(user.getLastName());
            txtBirthday.setText(user.getBirthday());
            txtPassword.setText(user.getPassword());
            txtConfirmPassword.setText(user.getPassword());
            if (user.getGender().equals("F")) {
                rbtFemale.setChecked(true);
            } else if (user.getGender().equals("M")) {
                rbtMale.setChecked(true);
            }
            txtDescription.setText(user.getDescription());

            Picasso.get().load(Constant.API_URL_USER + "/GetPhoto/" + user.getIdUser())
                    .error(R.drawable.no_user)
                    .placeholder(R.drawable.loading)
                    .into(imgPhoto, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            Log.e("ERROR", "ERROR", e);
                        }
                    });

            Picasso.get().load(Constant.API_URL_USER + "/GetBackgroundPhoto/" + user.getIdUser())
                    .error(R.drawable.no_photo_available)
                    .placeholder(R.drawable.loading)
                    .into(imgBackground, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            Log.e("ERROR", "ERROR", e);
                        }
                    });

            final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    // TODO Auto-generated method stub
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    String myFormat = "dd/MM/yyyy";
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                    txtBirthday.setText(sdf.format(myCalendar.getTime()));
                }

            };
            if (user.getBirthday() != null && !user.getBirthday().isEmpty()) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                myCalendar.setTime(sdf.parse(user.getBirthday()));// all done
            }
            txtBirthday.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new DatePickerDialog(getActivity(), date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });
        } catch (DAOException e) {
            Log.e("TAG", "Edit profile fragment", e);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        FloatingActionButton floatingActionButton = ((MainActivity) getActivity()).getFloatingActionButton();
        if (floatingActionButton != null) {
            floatingActionButton.hide();
        }
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.searchBar);
        if (item != null)
            item.setVisible(false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case REQUEST_CODE_CAMERA_PHOTO:
                if (resultCode == getActivity().RESULT_OK) {
                    Bitmap image = (Bitmap) imageReturnedIntent.getExtras().get("data");
                    imgPhoto.setImageBitmap(image);
                }
                break;
            case REQUEST_CODE_GALLERY_PHOTO:
                if (resultCode == getActivity().RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    imgPhoto.setImageURI(selectedImage);
                }
                break;
            case REQUEST_CODE_CAMERA_BACKGROUND:
                if (resultCode == getActivity().RESULT_OK) {
                    Bitmap image = (Bitmap) imageReturnedIntent.getExtras().get("data");
                    imgBackground.setImageBitmap(image);
                }
                break;
            case REQUEST_CODE_GALLERY_BACKGROUND:
                if (resultCode == getActivity().RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    imgBackground.setImageURI(selectedImage);
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, REQUEST_CODE_CAMERA_PHOTO);
                }
                break;
            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent cameraIntentBackground = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntentBackground, REQUEST_CODE_CAMERA_BACKGROUND);
                }
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibtnCameraPhoto:
                int permissionCheck = ContextCompat.checkSelfPermission(
                        getActivity(), Manifest.permission.CAMERA);
                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
                        requestPermissions(new String[]{
                                Manifest.permission.CAMERA, Manifest.permission.CAMERA}, 2);
                    } else {
                        requestPermissions(new String[]{
                                Manifest.permission.CAMERA, Manifest.permission.CAMERA}, 2);
                    }
                } else {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, REQUEST_CODE_CAMERA_PHOTO);
                }
                break;
            case R.id.ibtnGallerPhoto:
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,
                        "Seleccione una foto"), REQUEST_CODE_GALLERY_PHOTO);
                break;
            case R.id.ibtnCameraBackground:
                int permissionCheckBack = ContextCompat.checkSelfPermission(
                        getActivity(), Manifest.permission.CAMERA);
                if (permissionCheckBack != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
                        requestPermissions(new String[]{
                                Manifest.permission.CAMERA, Manifest.permission.CAMERA}, 1);
                    } else {
                        requestPermissions(new String[]{
                                Manifest.permission.CAMERA, Manifest.permission.CAMERA}, 1);
                    }
                } else {
                    Intent cameraIntentBackground = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntentBackground, REQUEST_CODE_CAMERA_BACKGROUND);
                }
                break;
            case R.id.ibtnGalleryBackground:
                Intent intentBackground = new Intent();
                intentBackground.setType("image/*");
                intentBackground.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intentBackground,
                        "Seleccione una foto"), REQUEST_CODE_GALLERY_BACKGROUND);
                break;
            case R.id.btnSave:
                if (txtName.getText().toString().trim().equals("")) {
                    uiMessages.showToast("Por favor ingresar su nombre", false);
                    txtName.requestFocus();
                } else if (txtPassword.getText().toString().trim().equals("")) {
                    uiMessages.showToast("Por favor ingresar el password", false);
                    txtPassword.requestFocus();
                } else if (txtConfirmPassword.getText().toString().equals("")) {
                    uiMessages.showToast("Por favor confirmar password", false);
                    txtConfirmPassword.requestFocus();
                } else if (!txtPassword.getText().toString().equals(txtConfirmPassword.getText().toString())) {
                    uiMessages.showToast("El password y confirmación de password deben ser iguales.", false);
                    txtConfirmPassword.requestFocus();
                } else {
                    User user = new User();
                    user.setEmail(txtEmail.getText().toString());
                    user.setName(txtName.getText().toString());
                    user.setLastName(txtLastname.getText().toString());
                    user.setBirthday(txtBirthday.getText().toString());
                    user.setPassword(txtPassword.getText().toString());
                    if (rbtFemale.isChecked()) {
                        user.setGender("F");
                    } else if (rbtMale.isChecked()) {
                        user.setGender("M");
                    }
                    user.setDescription(txtDescription.getText().toString());
                    user.setPhoto(AppUtils.getImgBase64(imgPhoto));
                    user.setBackgroundPhoto(AppUtils.getImgBase64(imgBackground));
                    new AysncUser().execute(new Gson().toJson(user));
                }
                break;
        }
    }

    private class AysncUser extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            uiMessages.showProgressDialog("Actualizando datos...", "Espere por favor");
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.post(Constant.API_URL_USER + "/Update", strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            uiMessages.dismissProgressDialog();
            User userUpdate = new Gson().fromJson(s, User.class);
            if (userUpdate != null) {
                UserDAO dao = new UserDAO(getActivity());
                try {
                    dao.insertar(userUpdate);
                    uiMessages.showToast("Se actualizaron los datos.", false);
                    Objects.requireNonNull(getActivity()).sendBroadcast(new Intent(MainActivity.RECEIVER_UPDATE_DATA));
                } catch (DAOException e) {
                    Log.i("EditProfile", "====> " + e.getMessage());
                }
            } else {
                uiMessages.showToast("No se pudo actualizar los datos, intentelo más tarde", false);
            }

        }
    }

    private void requestPermission(String permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{permissionName}, permissionRequestCode);
    }

}