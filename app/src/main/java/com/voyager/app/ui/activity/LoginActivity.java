package com.voyager.app.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.voyager.app.R;
import com.voyager.app.constant.Constant;
import com.voyager.app.data.UserDAO;
import com.voyager.app.data.sqliteUtil.DAOException;
import com.voyager.app.model.User;
import com.voyager.app.utils.OkHttpUtil;
import com.voyager.app.utils.UIMessages;

import java.io.IOException;


public class LoginActivity extends AppCompatActivity {
    private EditText txtEmail, txtPassword;
    private UIMessages uiMessages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        uiMessages = new UIMessages(this);
        overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
        UserDAO dao = new UserDAO(this);
        try {
            User userRegistered = dao.obtener();
            if (userRegistered != null) {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }

        txtEmail = findViewById(R.id.txtuserEmail);
        txtPassword = findViewById(R.id.txtUserPassword);
        txtPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    login(v);
                    return true;
                }
                return false;
            }
        });
        TextView txtVersion=findViewById(R.id.txtVersion);
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            txtVersion.setText(String.format("Version. %s",version));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void login(View view) {
        if (txtEmail.getText().toString().equals("")) {
            uiMessages.showToast("Por favor ingresar el email", false);
        } else if (txtPassword.getText().toString().equals("")) {
            uiMessages.showToast("Por favor ingresar el password", false);
        } else {
            User user = new User();
            user.setEmail(txtEmail.getText().toString());
            user.setPassword(txtPassword.getText().toString());
            new AysncUser().execute(new Gson().toJson(user));
        }

    }


    private class AysncUser extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            uiMessages.showProgressDialog("Iniciando sesión...", "Espere por favor");
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.post(Constant.API_URL_USER + "/Login", strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            uiMessages.dismissProgressDialog();
            User userLogin = new Gson().fromJson(s, User.class);
            if (userLogin != null) {
                UserDAO dao = new UserDAO(getBaseContext());
                try {
                    dao.insertar(userLogin);
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();
                } catch (DAOException e) {
                    Log.i("LoginActivity", "====> " + e.getMessage());
                }
            } else {
                uiMessages.showToast("Usuario o contraseña incorrectos", false);
            }

        }
    }

    public void registrar(View view) {
        startActivity(new Intent(this, NewUserActivity.class));
    }

    public void restorePassword(View view) {
        startActivity(new Intent(this, RestorePasswordActivity.class));
    }
}
