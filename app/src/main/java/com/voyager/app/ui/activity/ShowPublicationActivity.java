package com.voyager.app.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.voyager.app.R;
import com.voyager.app.constant.Constant;
import com.voyager.app.data.UserDAO;
import com.voyager.app.data.sqliteUtil.DAOException;
import com.voyager.app.model.Comment;
import com.voyager.app.model.Place;
import com.voyager.app.model.RatingPlace;
import com.voyager.app.model.SavePlace;
import com.voyager.app.model.User;
import com.voyager.app.model.ViewsPlace;
import com.voyager.app.ui.adapter.CommentAdapter;
import com.voyager.app.ui.adapter.SliderAdapter;
import com.voyager.app.utils.OkHttpUtil;
import com.voyager.app.utils.UIMessages;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ShowPublicationActivity extends AppCompatActivity implements View.OnClickListener, CommentAdapter.EventListener {
    private SupportMapFragment mapFragment;
    private List<Comment> commentList = new ArrayList<>();
    private RecyclerView recyclerView;
    private CommentAdapter mAdapter;
    private Place place;
    private ImageView imgLike, imgViews;
    private TextView txtRating, txtViews;
    private EditText txtWritteComent;
    private UIMessages uiMessages;
    private Context context;
    private ViewsPlace viewsPlace;
    private RatingPlace ratingPlace;
    private SwipeRefreshLayout refreshView, refreshOnly;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_publication);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        context = this;
        uiMessages = new UIMessages(context);
        place = new Gson().fromJson(getIntent().getStringExtra("placeData"), Place.class);
        SliderView sliderView = findViewById(R.id.imageSlider);
        SliderAdapter adapter = new SliderAdapter(this, place.getIdPhotos(), place.getName());
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimations.SWAP); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4); //set scroll delay in seconds :
        sliderView.startAutoCycle();
        TextView txtPlaceName = findViewById(R.id.txtPlaceName);
        TextView txtDescription = findViewById(R.id.txtDescription);
        Button btnSaveComment = findViewById(R.id.btnSaveComment);
        imgLike = findViewById(R.id.imgLike);
        imgViews = findViewById(R.id.imgViews);
        txtRating = findViewById(R.id.txtRating);
        txtViews = findViewById(R.id.txtViews);
        txtWritteComent = findViewById(R.id.txtWritteComent);
        btnSaveComment.setOnClickListener(this);
        imgLike.setOnClickListener(this);
        txtRating.setOnClickListener(this);
        txtPlaceName.setText(place.getName());
        txtDescription.setText(place.getDescription());
        txtViews.setText(place.getViews() + "");
        txtRating.setText(place.getRating() + "");
        refreshView = findViewById(R.id.refreshViewShowPublication);
        refreshOnly = findViewById(R.id.refreshOnly);
        TextView txtContactName = findViewById(R.id.txtContactName);
        TextView txtContactPhone = findViewById(R.id.txtContactPhone);
        LinearLayout llContant = findViewById(R.id.llContact);
        ImageView imgSavePlace = findViewById(R.id.imgSavePlace);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(place.getLatitude() + ""), Double.parseDouble(place.getLongitude() + ""))).title(place.getName()));
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(place.getLatitude() + ""), Double.parseDouble(place.getLongitude() + "")), 15));
            }
        });
        if (place.getContactPhone() != null && !place.getContactPhone().isEmpty()) {
            txtContactName.setText(place.getContactName());
            txtContactPhone.setText(place.getContactPhone());
        } else {
            llContant.setVisibility(LinearLayout.GONE);
        }
        refreshView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new AysncGetComents().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        refreshView.setRefreshing(true);
        refreshOnly.setRefreshing(true);
        new AysncGetComents().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        Button btnCall = findViewById(R.id.btnCall);
        btnCall.setOnClickListener(this);
        imgSavePlace.setOnClickListener(this);
    }

    private void requestPermission(String permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions(this,
                new String[]{permissionName}, permissionRequestCode);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AysncGetLikes().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new AysncPostViews().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgLike:
            case R.id.txtRating:
                if (ratingPlace != null) {
                    new AysncDeleteLike().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    new AysncPostLikes().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                break;
            case R.id.btnSaveComment:
                if (txtWritteComent.getText().toString().trim().equals("")) {
                    uiMessages.showToast("Escriba un comentario", false);
                    txtWritteComent.requestFocus();
                } else {
                    new AysncPostComment().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, txtWritteComent.getText().toString());
                }
                break;
            case R.id.btnCall:
                int permissionCheck = ContextCompat.checkSelfPermission(
                        this, Manifest.permission.CALL_PHONE);
                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)) {
                        requestPermission(Manifest.permission.CALL_PHONE, 1);
                    } else {
                        requestPermission(Manifest.permission.CALL_PHONE, 1);
                    }
                } else {
                    startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", place.getContactPhone(), null)));
                }
                break;
            case R.id.imgSavePlace:
                new AysncPostSave().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", place.getContactPhone(), null)));
                }
                break;
        }
    }

    @Override
    public void onEvent(int data) {
        new AysncGetComents().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class AysncGetLikes extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                UserDAO dao = new UserDAO(context);
                User user = dao.obtener();
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.get(Constant.API_URL_RATING_PLACE + "/Get/" + place.getIdPlace() + "/" + user.getIdUser());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DAOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            ratingPlace = new Gson().fromJson(s, RatingPlace.class);
            if (ratingPlace != null) {
                imgLike.setImageResource(R.drawable.heart_red);
            } else {
                imgLike.setImageResource(R.drawable.icon_heart);
            }
        }
    }

    private class AysncPostViews extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                UserDAO dao = new UserDAO(context);
                User user = dao.obtener();
                ViewsPlace viewsPlace = new ViewsPlace();
                viewsPlace.setIdPlace(place.getIdPlace());
                viewsPlace.setIdUser(Long.valueOf(user.getIdUser() + ""));
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.post(Constant.API_URL_VIEWS_PLACE + "/Create", new Gson().toJson(viewsPlace));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DAOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            viewsPlace = new Gson().fromJson(s, ViewsPlace.class);
            txtViews.setText(viewsPlace.getCountViews() + "");
        }
    }

    private class AysncPostComment extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            uiMessages.showProgressDialog("Guardando comentario..", "Espere por favor...");
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                UserDAO dao = new UserDAO(context);
                User user = dao.obtener();
                Comment comment = new Comment();
                comment.setIdUser(user.getIdUser());
                comment.setIdPlace(place.getIdPlace());
                comment.setDescription(strings[0]);
                String pattern = "dd/MM/yyyy HH:mm:ss";
                DateFormat df = new SimpleDateFormat(pattern);
                Date today = Calendar.getInstance().getTime();
                String todayAsString = df.format(today);
                comment.setDatetime(todayAsString);
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.post(Constant.API_URL_COMMENT + "/Create", new Gson().toJson(comment));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DAOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            uiMessages.dismissProgressDialog();
            txtWritteComent.setText("");
            refreshView.setRefreshing(true);
            refreshOnly.setRefreshing(true);
            new AysncGetComents().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private class AysncPostLikes extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                UserDAO dao = new UserDAO(context);
                User user = dao.obtener();
                RatingPlace ratingPlace = new RatingPlace();
                ratingPlace.setIdPlace(place.getIdPlace());
                ratingPlace.setIdUser(Long.valueOf(user.getIdUser() + ""));
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.post(Constant.API_URL_RATING_PLACE + "/Create", new Gson().toJson(ratingPlace));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DAOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            ratingPlace = new Gson().fromJson(s, RatingPlace.class);
            if (ratingPlace != null) {
                imgLike.setImageResource(R.drawable.heart_red);
            } else {
                imgLike.setImageResource(R.drawable.icon_heart);
            }
            txtRating.setText(ratingPlace.getCountLikes() + "");
        }
    }

    private class AysncDeleteLike extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.delete(Constant.API_URL_RATING_PLACE + "/Delete/" + ratingPlace.getIdRatingPlace() + "/" + place.getIdPlace());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            txtRating.setText(s);
            ratingPlace = null;
            imgLike.setImageResource(R.drawable.icon_heart);
        }
    }

    private class AysncGetComents extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.get(Constant.API_URL_COMMENT + "/GetAll/" + place.getIdPlace());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            refreshView.setRefreshing(false);
            refreshOnly.setRefreshing(false);
            commentList = new Gson().fromJson(s, new TypeToken<List<Comment>>() {
            }.getType());
            recyclerView = findViewById(R.id.rvComment);
            UserDAO dao = new UserDAO(context);
            User user = null;
            try {
                user = dao.obtener();
            } catch (DAOException e) {
                e.printStackTrace();
            }
            mAdapter = new CommentAdapter(commentList, context, ShowPublicationActivity.this, user.getIdUser() == place.getIdUser());
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
            recyclerView.setAdapter(mAdapter);
        }
    }

    private class AysncPostSave extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            uiMessages.showProgressDialog("Guardando sitio...", "Espere por favor...");
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                UserDAO dao = new UserDAO(context);
                User user = dao.obtener();
                SavePlace savePlace = new SavePlace();
                savePlace.setIdPlace(place.getIdPlace());
                savePlace.setIdUser(user.getIdUser());
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.post(Constant.API_URL_SAVE_PLACE + "/Create", new Gson().toJson(savePlace));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DAOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            uiMessages.dismissProgressDialog();
            uiMessages.showToast("Sitio guardado correctamente.", false);
        }
    }
}
