package com.voyager.app.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.voyager.app.R;
import com.voyager.app.constant.Constant;
import com.voyager.app.data.UserDAO;
import com.voyager.app.data.sqliteUtil.DAOException;
import com.voyager.app.model.Follows;
import com.voyager.app.model.Place;
import com.voyager.app.model.User;
import com.voyager.app.ui.adapter.PlaceAdapter;
import com.voyager.app.utils.OkHttpUtil;
import com.voyager.app.utils.UIMessages;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AnotherProfileActivity extends AppCompatActivity {
    private List<Place> placeList = new ArrayList<>();
    private RecyclerView recyclerView;
    private PlaceAdapter mAdapter;
    private User user;
    private TextView txtFollowersCount;
    private UIMessages uiMessages;
    private Context context;
//    private ScrollView mainScrollView;
    private SwipeRefreshLayout refreshView;
    private Button btnFollow;
    private Follows follows = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another_profile);
        user = new Gson().fromJson(getIntent().getStringExtra("userData"), User.class);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        uiMessages = new UIMessages(this);
        context = this;
        ImageView imgBackground = findViewById(R.id.imgBackground);
        CircularImageView civUserPhoto = findViewById(R.id.civUserPhoto);
        TextView txtUserName = findViewById(R.id.txtUserName);
        txtFollowersCount = findViewById(R.id.txtFollowersCount);
        TextView txtDescription = findViewById(R.id.txtDescription);
        btnFollow = findViewById(R.id.btnFollow);
        UserDAO userDAO = new UserDAO(context);
        User userFollow = null;
        try {
            userFollow = userDAO.obtener();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        if (userFollow != null && userFollow.getIdUser() == user.getIdUser()) {
            btnFollow.setVisibility(Button.GONE);
        }
        recyclerView = findViewById(R.id.recycler_view_otherProfile);
        txtUserName.setText(user.getName() + " " + user.getLastName());
//        mainScrollView = findViewById(R.id.mainScrollView);
        btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (follows != null) {
                    new AysncDelFollows().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    new AysncPostFollows().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });
        txtDescription.setText(user.getDescription());
        Picasso.get().load(Constant.API_URL_USER + "/GetPhoto/" + user.getIdUser())
                .error(R.drawable.no_user)
                .placeholder(R.drawable.loading)
                .into(civUserPhoto, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("EROR", "ERROR", e);
                    }
                });
        Picasso.get().load(Constant.API_URL_USER + "/GetBackgroundPhoto/" + user.getIdUser())
                .error(R.drawable.no_photo_available)
                .placeholder(R.drawable.loading)
                .into(imgBackground, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("ERROR", "ERROR", e);
                    }
                });
        refreshView = findViewById(R.id.refreshViewAnotherProfile);
        refreshView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new AysncPublications().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshView.setRefreshing(true);
        new AysncGetFollows().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new AysncPublications().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new AysncCountFollows().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//        mainScrollView.fullScroll(ScrollView.FOCUS_UP);
//        mainScrollView.smoothScrollTo(0, 0);
    }

    private class AysncCountFollows extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.get(Constant.API_URL_FOLLOWER + "/GetCount/" + user.getIdUser());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            txtFollowersCount.setText(s + " seguidores");
        }
    }

    private class AysncDelFollows extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.delete(Constant.API_URL_FOLLOWER + "/Delete/" + follows.getIdFollower());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.isEmpty()) {
                follows = null;
                btnFollow.setText("Seguir");
            }
            new AysncCountFollows().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private class AysncGetFollows extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                UserDAO userDAO = new UserDAO(context);
                User userFollow = userDAO.obtener();
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.get(Constant.API_URL_FOLLOWER + "/Get/" + user.getIdUser() + "/" + userFollow.getIdUser());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DAOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            follows = new Gson().fromJson(s, Follows.class);
            if (follows != null) {
//                btnFollow.
                btnFollow.setText("Dejar de seguir");
            } else {
                btnFollow.setText("Seguir");
            }
        }
    }

    private class AysncPostFollows extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                UserDAO userDAO = new UserDAO(context);
                User userFollow = userDAO.obtener();
                Follows follows = new Follows();
                follows.setIdUserFollower(Long.valueOf(userFollow.getIdUser() + ""));
                follows.setIdUser(Long.valueOf(user.getIdUser() + ""));
                String pattern = "dd/MM/yyyy HH:mm:ss";
                DateFormat df = new SimpleDateFormat(pattern);
                Date today = Calendar.getInstance().getTime();
                String todayAsString = df.format(today);
                follows.setDayFollowed(todayAsString);
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.post(Constant.API_URL_FOLLOWER + "/Create", new Gson().toJson(follows));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DAOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            follows = new Gson().fromJson(s, Follows.class);
            if (!s.isEmpty()) {
                btnFollow.setText("Dejar de seguir");
            }
            new AysncCountFollows().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private class AysncPublications extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.get(Constant.API_URL_PLACE + "/GetAllUser/" + user.getIdUser());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            refreshView.setRefreshing(false);
            if (s != null && !s.isEmpty()) {
                placeList = new Gson().fromJson(s, new TypeToken<List<Place>>() {
                }.getType());
                mAdapter = new PlaceAdapter(placeList, context,false);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
                recyclerView.setAdapter(mAdapter);
            } else {
                recyclerView.setAdapter(null);
                uiMessages.showToast("No se encontraron publicaciones", false);
            }
        }
    }
}
