package com.voyager.app.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.voyager.app.R;
import com.voyager.app.constant.Constant;
import com.voyager.app.data.UserDAO;
import com.voyager.app.data.sqliteUtil.DAOException;
import com.voyager.app.model.Place;
import com.voyager.app.model.SavePlace;
import com.voyager.app.model.User;
import com.voyager.app.ui.activity.MainActivity;
import com.voyager.app.ui.adapter.PlaceAdapter;
import com.voyager.app.utils.OkHttpUtil;
import com.voyager.app.utils.UIMessages;

import java.io.IOException;
import java.sql.Savepoint;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SavePlaceFragment extends Fragment {
    public static final String RECEVICER_SAVE_PLACE = "com.voyager.app.ui.receiver.RECEVICER_SAVE_PLACE";
    private List<Place> placeList = new ArrayList<>();
    private RecyclerView recyclerView;
    private PlaceAdapter mAdapter;
    private SwipeRefreshLayout refreshView;
    private UIMessages uiMessages;
    private ReceiverSavePlace receiverSavePlace;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_save_place, container, false);
        receiverSavePlace = new ReceiverSavePlace();
        uiMessages = new UIMessages(getActivity());
        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.searchBar);
        if (item != null)
            item.setVisible(false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = getActivity().findViewById(R.id.recycler_view_my_place_save);
        refreshView = getActivity().findViewById(R.id.refreshViewSavePlace);
        refreshView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new AysncPublications().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        FloatingActionButton floatingActionButton = ((MainActivity) getActivity()).getFloatingActionButton();
        if (floatingActionButton != null) {
            floatingActionButton.hide();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshView.setRefreshing(true);
        new AysncPublications().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        Objects.requireNonNull(getActivity()).registerReceiver(receiverSavePlace, new IntentFilter(RECEVICER_SAVE_PLACE));
    }

    @Override
    public void onPause() {
        super.onPause();
        Objects.requireNonNull(getActivity()).unregisterReceiver(receiverSavePlace);
    }

    private class AysncPublications extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            try {
                UserDAO dao = new UserDAO(getActivity());
                User user = dao.obtener();
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.get(Constant.API_URL_SAVE_PLACE + "/GetAll/" + user.getIdUser());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DAOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            refreshView.setRefreshing(false);
            if (s != null && !s.isEmpty()) {
                List<SavePlace> savePlaces = new Gson().fromJson(s, new TypeToken<List<SavePlace>>() {
                }.getType());
                placeList = new ArrayList<>();
                Place place;
                for (SavePlace savePlace : savePlaces) {
                    place = savePlace.getPlace();
                    place.setIdPlaceSave(savePlace.getIdSavePlace());
                    placeList.add(place);
                }
                mAdapter = new PlaceAdapter(placeList, getActivity(), true);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
                recyclerView.setAdapter(mAdapter);
            } else {
                recyclerView.setAdapter(null);
                uiMessages.showToast("No se encontraron seguidos", false);
            }
        }
    }

    private class ReceiverSavePlace extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            onResume();
        }
    }
}