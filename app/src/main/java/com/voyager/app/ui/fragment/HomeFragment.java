package com.voyager.app.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.voyager.app.R;
import com.voyager.app.constant.Constant;
import com.voyager.app.data.UserDAO;
import com.voyager.app.data.sqliteUtil.DAOException;
import com.voyager.app.model.Place;
import com.voyager.app.model.User;
import com.voyager.app.ui.activity.MainActivity;
import com.voyager.app.ui.adapter.PlaceAdapter;
import com.voyager.app.utils.OkHttpUtil;
import com.voyager.app.utils.UIMessages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {
    public static final String RECEIVER_UPDATE_PUBLICATIONS = "com.voyager.app.receiver.ReceiverRefreshPublication";
    private List<Place> placeList = new ArrayList<>();
    private RecyclerView recyclerView;
    private PlaceAdapter mAdapter;
    private SwipeRefreshLayout refreshView;
    private UIMessages uiMessages;
    private ReceiverRefreshPublication receiverRefreshPublication;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        uiMessages = new UIMessages(getContext());
        receiverRefreshPublication = new ReceiverRefreshPublication();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = getActivity().findViewById(R.id.recycler_view_home);
        refreshView = getActivity().findViewById(R.id.refreshViewHome);
        refreshView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new AysncPublications().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
            }
        });
        FloatingActionButton floatingActionButton = ((MainActivity) getActivity()).getFloatingActionButton();
        if (floatingActionButton != null) {
            floatingActionButton.show();
        }
        refreshPublications("");
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(receiverRefreshPublication, new IntentFilter(RECEIVER_UPDATE_PUBLICATIONS));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiverRefreshPublication);
    }

    public void refreshPublications(String codition) {
        refreshView.setRefreshing(true);
        new AysncPublications().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, codition);
    }

    private class AysncPublications extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            try {
                UserDAO dao = new UserDAO(getActivity());
                User user = dao.obtener();
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                String response = "";
                if (strings[0].isEmpty()) {
                    response = okHttpUtil.get(Constant.API_URL_PLACE + "/GetAll/" + user.getIdUser());
                } else {
                    response = okHttpUtil.get(Constant.API_URL_PLACE + "/GeSearchPlace/" + strings[0]);
                }
                return response;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DAOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            refreshView.setRefreshing(false);
            if (s != null && !s.isEmpty()) {
                placeList = new Gson().fromJson(s, new TypeToken<List<Place>>() {
                }.getType());
                mAdapter = new PlaceAdapter(placeList, getActivity(),false);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
                recyclerView.setAdapter(mAdapter);
            } else {
                recyclerView.setAdapter(null);
                uiMessages.showToast("No se encontraron publicaciones", false);
            }
        }
    }

    private class ReceiverRefreshPublication extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            refreshPublications(intent.getStringExtra("condition"));
        }
    }
}