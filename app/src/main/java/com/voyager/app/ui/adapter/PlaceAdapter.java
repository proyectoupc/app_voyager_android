package com.voyager.app.ui.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.voyager.app.R;
import com.voyager.app.constant.Constant;
import com.voyager.app.model.Comment;
import com.voyager.app.model.Place;
import com.voyager.app.ui.activity.AnotherProfileActivity;
import com.voyager.app.ui.activity.ShowPublicationActivity;
import com.voyager.app.ui.fragment.SavePlaceFragment;
import com.voyager.app.utils.OkHttpUtil;
import com.voyager.app.utils.UIMessages;

import java.io.IOException;
import java.util.List;

public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.ViewHolder> {
    private List<Place> listPlace;
    private Context context;
    private boolean savedPlace = false;
    private UIMessages uiMessages;

    public PlaceAdapter(List<Place> listPlace, Context context, boolean savedPlace) {
        this.listPlace = listPlace;
        this.context = context;
        this.savedPlace = savedPlace;
        uiMessages = new UIMessages(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_place, parent, false);
        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Place place = listPlace.get(position);
        holder.txtUserName.setText(place.getUser().getName() + " " + place.getUser().getLastName());
        holder.txtPlaceTitle.setText(place.getName());
        holder.txtPlaceDescription.setText(place.getDescription());
        holder.llContentPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Place place1 = listPlace.get(holder.getAdapterPosition());
                context.startActivity(new Intent(context, ShowPublicationActivity.class).putExtra("placeData", new Gson().toJson(place1)));
            }
        });
        Picasso.get().load(Constant.API_URL_USER + "/GetPhoto/" + place.getUser().getIdUser())
                .error(R.drawable.no_user)
                .placeholder(R.drawable.cargar)
                .into(holder.civUserPhoto, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("EROR", "ERROR", e);
                    }
                });
        int idPhotoPlace = 0;
        if (place.getIdPhotos() != null && place.getIdPhotos().size() > 0) {
            idPhotoPlace = place.getIdPhotos().get(0);
        }
        Picasso.get().load(Constant.API_URL_PHOTO + "/GetPhoto/" + idPhotoPlace)
                .error(R.drawable.no_photo_available)
                .placeholder(R.drawable.cargar)
                .into(holder.imgPhotoPlace, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("EROR", "ERROR", e);
                    }
                });
        holder.civUserPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Place placeSel = listPlace.get(holder.getAdapterPosition());
                context.startActivity(new Intent(context, AnotherProfileActivity.class).putExtra("userData", new Gson().toJson(placeSel.getUser())));
            }
        });
        holder.txtUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Place placeSel = listPlace.get(holder.getAdapterPosition());
                context.startActivity(new Intent(context, AnotherProfileActivity.class).putExtra("userData", new Gson().toJson(placeSel.getUser())));
            }
        });
        if (savedPlace) {
            holder.imgDeletePlaceSave.setVisibility(ImageView.VISIBLE);
            holder.imgDeletePlaceSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    uiMessages.showAlertDialog("ADVERTENCIA!!!", "¿Está seguro que desea eliminar?", "SI", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Place placeSel = listPlace.get(holder.getAdapterPosition());
                            new AsyncDelSavePlace().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, placeSel.getIdPlaceSave() + "");
                        }
                    }, "NO", null, null, true);
                }
            });
        } else {
            holder.imgDeletePlaceSave.setVisibility(ImageView.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return listPlace.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtUserName, txtPlaceTitle, txtPlaceDescription;
        public LinearLayout llPlace, llContentPlace;
        public com.mikhaellopez.circularimageview.CircularImageView civUserPhoto;
        public ImageView imgPhotoPlace, imgDeletePlaceSave;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtUserName = itemView.findViewById(R.id.txtUserName);
            txtPlaceTitle = itemView.findViewById(R.id.txtPlaceTitle);
            txtPlaceDescription = itemView.findViewById(R.id.txtPlaceDescription);
            llPlace = itemView.findViewById(R.id.llPlace);
            civUserPhoto = itemView.findViewById(R.id.civUserPhoto);
            llContentPlace = itemView.findViewById(R.id.llContentPlace);
            imgPhotoPlace = itemView.findViewById(R.id.imgPhotoPlace);
            imgDeletePlaceSave = itemView.findViewById(R.id.imgDeletePlaceSave);
        }

    }

    private class AsyncDelSavePlace extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            uiMessages.showProgressDialog("Eliminando...", "Espere por favor");
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.delete(Constant.API_URL_SAVE_PLACE + "/Delete/" + strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            uiMessages.dismissProgressDialog();
            context.sendBroadcast(new Intent(SavePlaceFragment.RECEVICER_SAVE_PLACE));
        }
    }
}
