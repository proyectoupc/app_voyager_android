package com.voyager.app.ui.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.voyager.app.R;
import com.voyager.app.constant.Constant;
import com.voyager.app.data.UserDAO;
import com.voyager.app.data.sqliteUtil.DAOException;
import com.voyager.app.model.Place;
import com.voyager.app.model.User;
import com.voyager.app.ui.activity.MainActivity;
import com.voyager.app.ui.adapter.PlaceAdapter;
import com.voyager.app.utils.OkHttpUtil;
import com.voyager.app.utils.UIMessages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MyProfileFragment extends Fragment {
    private List<Place> placeList = new ArrayList<>();
    private RecyclerView recyclerView;
    private PlaceAdapter mAdapter;
//    private ScrollView mainScrollView;
    private User user;
    private TextView txtFollowersCount;
    private UIMessages uiMessages;
    private SwipeRefreshLayout refreshView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        uiMessages = new UIMessages(getActivity());
        View root = inflater.inflate(R.layout.fragment_my_profile, container, false);
        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
//        mainScrollView.fullScroll(ScrollView.FOCUS_UP);
//        mainScrollView.smoothScrollTo(0, 0);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.searchBar);
        if (item != null)
            item.setVisible(false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        try {
            recyclerView = getActivity().findViewById(R.id.recycler_view_MyProfile);
            refreshView = getActivity().findViewById(R.id.refreshViewMyProfile);
            refreshView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    new AysncPublications().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            });
//            mainScrollView = getActivity().findViewById(R.id.mainScrollView);

            ImageView imgBackground = getActivity().findViewById(R.id.imgBackgroundMyProfile);
            CircularImageView civUserPhoto = getActivity().findViewById(R.id.civUserPhotoP);
            TextView txtUserName = getActivity().findViewById(R.id.txtUserNameP);
            txtFollowersCount = getActivity().findViewById(R.id.txtFollowersCount);
            TextView txtDescription = getActivity().findViewById(R.id.txtDescription);
            UserDAO userDAO = new UserDAO(getActivity());

            user = userDAO.obtener();
            txtUserName.setText(user.getName() + " " + user.getLastName());
            txtDescription.setText(user.getDescription());

            Picasso.get().load(Constant.API_URL_USER + "/GetPhoto/" + user.getIdUser())
                    .error(R.drawable.no_user)
                    .placeholder(R.drawable.loading)
                    .into(civUserPhoto, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            Log.e("EROR", "ERROR", e);
                        }
                    });
            Picasso.get().load(Constant.API_URL_USER + "/GetBackgroundPhoto/" + user.getIdUser())
                    .error(R.drawable.no_photo_available)
                    .placeholder(R.drawable.loading)
                    .into(imgBackground, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            Log.e("EROR", "ERROR", e);
                        }
                    });
            new AysncFollows().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            refreshView.setRefreshing(true);
            new AysncPublications().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        } catch (DAOException e) {
            e.printStackTrace();
        }

        FloatingActionButton floatingActionButton = ((MainActivity) getActivity()).getFloatingActionButton();
        if (floatingActionButton != null) {
            floatingActionButton.hide();
        }
    }

    private class AysncFollows extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.get(Constant.API_URL_FOLLOWER + "/GetCount/" + user.getIdUser());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            txtFollowersCount.setText(s + " seguidores");
        }
    }

    private class AysncPublications extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            try {
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.get(Constant.API_URL_PLACE + "/GetAllUser/" + user.getIdUser());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            refreshView.setRefreshing(false);
            if (s != null && !s.isEmpty()) {
                placeList = new Gson().fromJson(s, new TypeToken<List<Place>>() {
                }.getType());
                mAdapter = new PlaceAdapter(placeList, getActivity(), false);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
                recyclerView.setAdapter(mAdapter);
            } else {
                recyclerView.setAdapter(null);
                uiMessages.showToast("No se encontraron publicaciones", false);
            }
        }
    }

}