package com.voyager.app.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.voyager.app.R;
import com.voyager.app.constant.Constant;
import com.voyager.app.data.UserDAO;
import com.voyager.app.data.sqliteUtil.DAOException;
import com.voyager.app.model.User;
import com.voyager.app.ui.fragment.HomeFragment;
import com.voyager.app.utils.OkHttpUtil;
import com.voyager.app.utils.UIMessages;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    public static final String RECEIVER_UPDATE_DATA = "com.voyager.app.ui.receiver.RECEIVER_UPDATE_DATA";
    private static final String TAG = MainActivity.class.getName();
    private Context context;
    private AppBarConfiguration mAppBarConfiguration;
    public UIMessages uiMessages;
    private FloatingActionButton fab;
    private ImageView civUserPhotoNav, imgBackgroundNav;
    private TextView txtName, txtEmail;
    private ReceiverLoadDatas receiverLoadDatas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        receiverLoadDatas = new ReceiverLoadDatas();
        uiMessages = new UIMessages(context);
        setContentView(R.layout.activity_main);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, NewPlaceActivity.class));
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_my_profile, R.id.nav_edit_profile,
                R.id.nav_show_follows, R.id.nav_show_followers, R.id.nav_save_place)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        navigationView.getMenu().findItem(R.id.nav_exit).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                try {
                    UserDAO dao = new UserDAO(context);
                    dao.eliminar();
                } catch (DAOException e) {
                    e.printStackTrace();
                }
                startActivity(new Intent(context, LoginActivity.class));
                finish();
                return false;
            }
        });
        View headerView = navigationView.getHeaderView(0);
        txtName = headerView.findViewById(R.id.txtUserName);
        txtEmail = headerView.findViewById(R.id.txtuserEmail);
        civUserPhotoNav = headerView.findViewById(R.id.civUserPhotoNav);
        imgBackgroundNav = headerView.findViewById(R.id.imgBackgroundNav);
        loadUserData();
        TextView txtVersion=findViewById(R.id.txtVersion);
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            txtVersion.setText(String.format("Version. %s",version));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiverLoadDatas, new IntentFilter(RECEIVER_UPDATE_DATA));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiverLoadDatas);
    }

    private void loadUserData() {
        try {
            UserDAO DAO = new UserDAO(getBaseContext());
            final User user = DAO.obtener();
            txtName.setText(user.getName() + " " + user.getLastName());
            txtEmail.setText(user.getEmail());
            Picasso.get().invalidate(Constant.API_URL_USER + "/GetPhoto/" + user.getIdUser());
            Picasso.get().load(Constant.API_URL_USER + "/GetPhoto/" + user.getIdUser()).error(R.drawable.no_user).memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).networkPolicy(NetworkPolicy.NO_CACHE).placeholder(R.drawable.loading).into(civUserPhotoNav);
            Picasso.get().invalidate(Constant.API_URL_USER + "/GetBackgroundPhoto/" + user.getIdUser());
            Picasso.get().load(Constant.API_URL_USER + "/GetBackgroundPhoto/" + user.getIdUser()).error(R.drawable.no_photo_available).memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).networkPolicy(NetworkPolicy.NO_CACHE).placeholder(R.drawable.loading).into(imgBackgroundNav);
            Log.d("FCMToken", "token " + FirebaseInstanceId.getInstance().getToken());
            FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                @Override
                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                    if (!task.isSuccessful()) {
                        Log.w(TAG, "getInstanceId failed", task.getException());
                        return;
                    }
                    String token = Objects.requireNonNull(task.getResult()).getToken();
                    user.setToken(token);
                    Log.d(TAG, token);
                    new AysncToken().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Gson().toJson(user));
                }
            });
        } catch (DAOException e) {
            Log.e(TAG, "Error", e);
        }
    }

    public FloatingActionButton getFloatingActionButton() {
        return fab;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.searchBar);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint("Buscar...");
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_exit) {
            try {
                UserDAO dao = new UserDAO(context);
                dao.eliminar();
            } catch (DAOException e) {
                e.printStackTrace();
            }
            startActivity(new Intent(context, LoginActivity.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        sendBroadcast(new Intent(HomeFragment.RECEIVER_UPDATE_PUBLICATIONS).putExtra("condition", s));
        Log.d("TAG", s);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        Log.d("TAG", s);
        if (s.isEmpty()) {
            sendBroadcast(new Intent(HomeFragment.RECEIVER_UPDATE_PUBLICATIONS).putExtra("condition", s));
        }
        return false;
    }

    private class AysncToken extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.post(Constant.API_URL_USER + "/UpdateToken", strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private class ReceiverLoadDatas extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            loadUserData();
        }
    }
}