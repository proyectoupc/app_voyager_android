package com.voyager.app.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smarteist.autoimageslider.SliderViewAdapter;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.voyager.app.R;
import com.voyager.app.constant.Constant;

import java.util.List;


public class SliderAdapter extends SliderViewAdapter<SliderAdapter.ViewHolder> {

    private Context context;
    private List<Integer> idPhotos;
    private String placeName;

    public SliderAdapter(Context context, List<Integer> idPhotos, String placeName) {
        this.context = context;
        this.idPhotos = idPhotos;
        this.placeName = placeName;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
        return new ViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.textViewDescription.setText(placeName);
        Picasso.get().load(Constant.API_URL_PHOTO + "/GetPhoto/" + idPhotos.get(position))
                .error(R.drawable.no_photo_available)
                .placeholder(R.drawable.loading)
                .into(viewHolder.imageViewBackground, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("EROR", "ERROR", e);
                    }
                });

    }

    @Override
    public int getCount() {
        return idPhotos.size();
    }

    class ViewHolder extends SliderViewAdapter.ViewHolder {
        View itemView;
        ImageView imageViewBackground;
        TextView textViewDescription;

        public ViewHolder(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider);
            this.itemView = itemView;
        }
    }
}