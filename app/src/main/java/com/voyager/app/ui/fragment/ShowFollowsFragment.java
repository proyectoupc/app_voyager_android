package com.voyager.app.ui.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.voyager.app.R;
import com.voyager.app.constant.Constant;
import com.voyager.app.data.UserDAO;
import com.voyager.app.data.sqliteUtil.DAOException;
import com.voyager.app.model.Follows;
import com.voyager.app.model.User;
import com.voyager.app.ui.activity.MainActivity;
import com.voyager.app.ui.adapter.FollowsAdapter;
import com.voyager.app.utils.OkHttpUtil;
import com.voyager.app.utils.UIMessages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ShowFollowsFragment extends Fragment implements FollowsAdapter.EventListener {
    private List<Follows> followsList = new ArrayList<>();
    private RecyclerView recyclerView;
    private FollowsAdapter mAdapter;
    private UIMessages uiMessages;
    private SwipeRefreshLayout refreshView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_show_follows, container, false);
        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.searchBar);
        if (item != null)
            item.setVisible(false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = getActivity().findViewById(R.id.recycler_viewFollows);
        uiMessages = new UIMessages(getActivity());
        refreshView = getActivity().findViewById(R.id.refreshFollows);
        refreshView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new AysncUser().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        loadFollows();
        FloatingActionButton floatingActionButton = ((MainActivity) getActivity()).getFloatingActionButton();
        if (floatingActionButton != null) {
            floatingActionButton.hide();
        }
    }

    private void loadFollows() {
        refreshView.setRefreshing(true);
        new AysncUser().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onEvent(int data) {
        loadFollows();
    }

    private class AysncUser extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            try {
                UserDAO dao = new UserDAO(getActivity());
                User user = dao.obtener();
                OkHttpUtil okHttpUtil = new OkHttpUtil();
                return okHttpUtil.get(Constant.API_URL_FOLLOWER + "/GetAllFollows/" + user.getIdUser());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DAOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            refreshView.setRefreshing(false);
            if (s != null && !s.isEmpty()) {
                followsList = new Gson().fromJson(s, new TypeToken<List<Follows>>() {
                }.getType());
                mAdapter = new FollowsAdapter(followsList, getContext(), ShowFollowsFragment.this);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
                recyclerView.setAdapter(mAdapter);

            } else {
                recyclerView.setAdapter(null);
                uiMessages.showToast("No se encontraron seguidos", false);
            }
        }
    }
}