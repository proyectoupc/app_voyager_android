package com.voyager.app.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.voyager.app.R;
import com.voyager.app.constant.Constant;
import com.voyager.app.model.Photo;
import com.voyager.app.ui.activity.MainActivity;
import com.voyager.app.utils.OkHttpUtil;
import com.voyager.app.utils.SharedPreferencesUtils;

import java.io.IOException;
import java.util.List;

public class SavePhotosService extends IntentService {
    private NotificationManager notificationManager;
    private NotificationCompat.Builder notificationBuilder;

    public SavePhotosService() {
        super(SavePhotosService.class.getName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        SharedPreferencesUtils sharedPreferencesUtils = new SharedPreferencesUtils(this);
        List<Photo> photos = sharedPreferencesUtils.LoadDataPhotos();
        if (photos != null && photos.size() > 0) {
            createNotification();
            OkHttpUtil okHttpUtil = new OkHttpUtil();
            int response;
            for (int i = 0; i < photos.size(); i++) {
                try {
                    response = Integer.valueOf(okHttpUtil.post(Constant.API_URL_PHOTO + "/Create", new Gson().toJson(photos.get(i))));
                    if (response > 0) {
                        progressNotification(photos.size(), i + 1, false);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            progressNotification(photos.size(), photos.size(), true);
        }
        sharedPreferencesUtils.RemoveDataPhotos();
    }

    private void createNotification() {
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "my_channel_id_01";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("my_channel_id_01", "my_channel_id_01", importance);
            channel.setDescription("my_channel_id_01");
            notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
        Bitmap myBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.voyager_logo);
        notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent actionPendingIntent =
                PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setAutoCancel(false).setDefaults(Notification.DEFAULT_ALL)
                .setContentIntent(actionPendingIntent)
                .setLargeIcon(myBitmap)
                .setSmallIcon(R.drawable.voyager_logo)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_LARGE)
                .setContentTitle("Iniciando carga de fotos..")
                .setContentText("").setAutoCancel(true).setContentInfo("SMART");
        notificationManager.notify(1, notificationBuilder.build());
    }

    private void progressNotification(int max, int progress, boolean end) {
        if (end) {
            notificationBuilder.setContentTitle("Se cargaron las fotos " + +max + "/" + progress)
                    .setProgress(0, 0, false);
            notificationManager.notify(1, notificationBuilder.build());
        } else {
            notificationBuilder.setProgress(max, progress, false)
                    .setContentTitle("Se están cargando las fotos " + max + "/" + progress);
            notificationManager.notify(/*notification id*/1, notificationBuilder.build());
        }
    }
}