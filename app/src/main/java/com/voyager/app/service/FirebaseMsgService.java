package com.voyager.app.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.voyager.app.R;
import com.voyager.app.model.NotificationFB;
import com.voyager.app.ui.activity.ShowPublicationActivity;

import org.jetbrains.annotations.NotNull;

import java.util.Map;


public class FirebaseMsgService extends FirebaseMessagingService {
    @Override
    public void onNewToken(@NotNull String token) {
        super.onNewToken(token);
        Log.d("====>", "NEW_TOKEN: " + token);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Map<String, String> data = remoteMessage.getData();
        String myCustomKey = data.get("my_custom_key");
        createNotification(myCustomKey);
    }

    private void createNotification(String messageBody) {

        NotificationFB notificationData = new Gson().fromJson(messageBody, NotificationFB.class);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "my_channel_id_01";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("my_channel_id_01", "my_channel_id_01", importance);
            channel.setDescription("my_channel_id_01");
            notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
        Bitmap myBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.voyager_logo);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Intent intent = new Intent(this, ShowPublicationActivity.class);
        intent.putExtra("placeData", new Gson().toJson(notificationData.getPlace()));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent actionPendingIntent =
                PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setAutoCancel(true).setDefaults(Notification.DEFAULT_ALL)
                .setContentIntent(actionPendingIntent)
                .setLargeIcon(myBitmap)
                .setSmallIcon(R.drawable.voyager_logo)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_LARGE)
                .setContentTitle(String.format("%s %s comentó en tu publicación", notificationData.getUser().getName(), notificationData.getUser().getLastName()))
                .setContentText(notificationData.getComment().getDescription()).setAutoCancel(true).setContentInfo("SMART");
        notificationManager.notify(Integer.valueOf(notificationData.getComment().getIdComentario() + ""), notificationBuilder.build());
    }
}