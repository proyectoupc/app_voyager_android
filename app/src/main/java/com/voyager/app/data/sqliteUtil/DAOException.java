package com.voyager.app.data.sqliteUtil;

public class DAOException extends Exception{

    public DAOException(String detailMessage) { super(detailMessage); }
}
