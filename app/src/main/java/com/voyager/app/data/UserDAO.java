package com.voyager.app.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.voyager.app.data.sqliteUtil.DAOException;
import com.voyager.app.data.sqliteUtil.DbHelper;
import com.voyager.app.model.User;

import java.util.ArrayList;

public class UserDAO {
    private DbHelper _dbHelper;

    public UserDAO(Context c) {
        _dbHelper = new DbHelper(c);
    }

    public void insertar(User user) throws DAOException {
        Log.i("UserDAO", "insertar()");
        SQLiteDatabase db = _dbHelper.getWritableDatabase();
        try {
            String[] args = new String[]{user.getIdUser() + "", user.getEmail(), user.getPassword(), user.getName(), user.getLastName(), user.getBirthday(), user.getGender(), user.getDescription()};
            db.execSQL("INSERT INTO user(idUser, email, password, name, lastName, birthday, gender, description) VALUES(?,?,?,?,?,?,?,?)", args);
            Log.i("UserDAO", "Se insertó correctamente");
        } catch (Exception e) {
            throw new DAOException("UserDAO: Error al insertar: " + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    public User obtener() throws DAOException {
        Log.i("UserDAO", "obtener()");
        SQLiteDatabase db = _dbHelper.getReadableDatabase();
        User modelo = null;
        try {
            Cursor c = db.rawQuery("select id, idUser, email, password, name, lastName, birthday, gender, description from user", null);
            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    modelo = new User();
                    int id = c.getInt(c.getColumnIndex("id"));
                    int idUser = c.getInt(c.getColumnIndex("idUser"));
                    String email = c.getString(c.getColumnIndex("email"));
                    String password = c.getString(c.getColumnIndex("password"));
                    String name = c.getString(c.getColumnIndex("name"));
                    String lastName = c.getString(c.getColumnIndex("lastName"));
                    String birthday = c.getString(c.getColumnIndex("birthday"));
                    String gender = c.getString(c.getColumnIndex("gender"));
                    String description = c.getString(c.getColumnIndex("description"));

                    modelo.set_id(id);
                    modelo.setIdUser(idUser);
                    modelo.setEmail(email);
                    modelo.setPassword(password);
                    modelo.setName(name);
                    modelo.setLastName(lastName);
                    modelo.setBirthday(birthday);
                    modelo.setGender(gender);
                    modelo.setDescription(description);

                } while (c.moveToNext());
            }
            c.close();
        } catch (Exception e) {
            throw new DAOException("UserDAO: Error al obtener: " + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return modelo;

    }

    public void eliminar() throws DAOException {
        Log.i("UserDAO", "eliminar()");
        SQLiteDatabase db = _dbHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM user" );
        } catch (Exception e) {
            throw new DAOException("UserDAO: Error al eliminar: " + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

}