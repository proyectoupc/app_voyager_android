package com.voyager.app.utils;

import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class OkHttpUtil {
    private static final String TAG = OkHttpUtil.class.getName();
    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    private OkHttpClient client = new OkHttpClient();

    public String post(String url, String json) throws IOException {
        Log.d(TAG, url);
        Log.d(TAG, json);
        RequestBody body = RequestBody.create(json, JSON);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        try (Response response = client.newCall(request).execute()) {
            String strResponse = Objects.requireNonNull(response.body()).string();
            Log.d(TAG, strResponse);
            return strResponse;
        }
    }

    public String delete(String url) throws IOException {
        Log.d(TAG, url);
        Request request = new Request.Builder()
                .url(url)
                .delete()
                .build();
        try (Response response = client.newCall(request).execute()) {
            String strResponse = Objects.requireNonNull(response.body()).string();
            Log.d(TAG, strResponse);
            return strResponse;
        }
    }

    public String get(String url) throws IOException {
        Log.d(TAG, url);
        Request request = new Request.Builder()
                .url(url).build();
        try (Response response = client.newCall(request).execute()) {
            String strResponse = Objects.requireNonNull(response.body()).string();
            Log.d(TAG, strResponse);
            return strResponse;
        }
    }
}
