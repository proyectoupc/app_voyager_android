package com.voyager.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.voyager.app.R;
import com.voyager.app.model.Photo;

import java.util.List;

public class SharedPreferencesUtils {
    private Context context;

    public SharedPreferencesUtils(Context context) {
        this.context = context;
    }

    private SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(
                context.getResources().getString(R.string.sharedpreferences_filename),
                Context.MODE_PRIVATE);
    }
    public boolean SaveDataPhotos(String photos) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("photoList", photos);
        return editor.commit();
    }
    public List<Photo> LoadDataPhotos() {
        SharedPreferences sharedPreferences = getSharedPreferences();
        return new Gson().fromJson(sharedPreferences.getString("photoList", ""),new TypeToken<List<Photo>>() {
        }.getType());
    }
    public boolean RemoveDataPhotos() {
        SharedPreferences sharedPreferences = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove("photoList");
        return editor.commit();
    }
}
