package com.voyager.app.utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.SpannableString;
import android.text.util.Linkify;
import android.view.Gravity;
import android.view.KeyEvent;
import android.widget.Toast;

/**
 * Esta clase es utilizada para mostrar mensajes en la interfaz de usuario.
 * Entre los mensajes que con esta clase se pueden mostrar est|�n los cuadros de
 * alerta, cuadros de espera, entre otros.
 */
public class UIMessages {
    private Context context;

    private ProgressDialog progressDialog;
    private AlertDialog alertDialog;
    private Toast toast;

    /**
     * Construye una instancia de la clase {@code UIMessages} con los par�metros
     * especificados.
     *
     * @param context Objeto {@code Context} de la aplicaci�n.
     */
    public UIMessages(Context context) {
        this.context = context;
        progressDialog = null;
        alertDialog = null;
        toast = null;
    }

    /**
     * Muestra un cuadro tipo {@code ProgressDialog} en la interfaz de usuario.
     *
     * @param title   T�tulo del cuadro de progreso.
     * @param message Mensaje del cuadro de progreso.
     */
    public void showProgressDialog(String title, String message) {
        dismissProgressDialog();
        progressDialog = ProgressDialog.show(context, title, message);
    }

    /**
     * Cierra el cuadro de progreso en caso est� activo.
     */
    public void dismissProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    /**
     * Muestra un di�logo tipo {@code AlertDialog} en la interfaz de usuario.
     *
     * @param title                 T�tulo del di�logo de alerta.
     * @param message               Mensaje del di�logo de alerta.
     * @param positiveButtonMessage Mensaje del bot�n positivo del di�logo de alerta.
     * @param postiveButtonListener Implementaci�n de la interfaz
     *                              {@code DialogInterface.OnClickListener} con los procedimientos
     *                              a realizar en caso se seleccione el bot�n positivo.
     * @param icon                  Objeto {@code Drawable} con el icono. Ingresar {@code null} si
     *                              no se desea un icono.
     * @param isCancelable          Booleano que indica si el di�logo de alerta puede cerrarse al
     *                              presionar el bot�n {@link KeyEvent#KEYCODE_BACK BACK} del
     *                              dispositivo. Ingresar verdadero para que pueda cerrarse o
     *                              falso para que no.
     */
    public void showAlertDialog(String title, String message,
                                String positiveButtonMessage,
                                DialogInterface.OnClickListener postiveButtonListener,
                                Drawable icon, boolean isCancelable) {

        showAlertDialog(title, message, positiveButtonMessage,
                postiveButtonListener, null, null, icon, isCancelable);
    }

    /**
     * Muestra un di�logo tipo {@code AlertDialog} en la interfaz de usuario.
     *
     * @param title                  T�tulo del di�logo de alerta.
     * @param message                Mensaje del di�logo de alerta.
     * @param positiveButtonMessage  Mensaje del bot�n positivo del di�logo de alerta.
     * @param postiveButtonListener  Implementaci�n de la interfaz
     *                               {@code DialogInterface.OnClickListener} con los procedimientos
     *                               a realizar en caso se seleccione el bot�n positivo.
     * @param negativeButtonMessage  Mensaje del bot�n negativo del di�logo de alerta.
     * @param negativeButtonListener Implementaci�n de la interfaz
     *                               {@code DialogInterface.OnClickListener} con los procedimientos
     *                               a realizar en caso se seleccione el bot�n negativo.
     * @param icon                   Objeto {@code Drawable} con el icono. Ingresar {@code null} si
     *                               no se desea un icono.
     * @param isCancelable           Booleano que indica si el di�logo de alerta puede cerrarse al
     *                               presionar el bot�n {@link KeyEvent#KEYCODE_BACK BACK} del
     *                               dispositivo. Ingresar verdadero para que pueda cerrarse o
     *                               falso para que no.
     */
    public void showAlertDialog(String title, String message,
                                String positiveButtonMessage,
                                DialogInterface.OnClickListener postiveButtonListener,
                                String negativeButtonMessage,
                                DialogInterface.OnClickListener negativeButtonListener,
                                Drawable icon, boolean isCancelable) {

        dismissAlertDialog();
        final SpannableString spannableString = new SpannableString(Html.fromHtml(message));
        Linkify.addLinks(spannableString, Linkify.ALL);

        alertDialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(spannableString.toString())
                .setPositiveButton(positiveButtonMessage, postiveButtonListener)
                .setNegativeButton(negativeButtonMessage,
                        negativeButtonListener).setIcon(icon)
                .setCancelable(isCancelable).create();
        alertDialog.show();
    }

    /**
     * Cierra el di�logo de alerta en caso est� activo.
     */
    public void dismissAlertDialog() {
        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }

    /**
     * Muestra un cuadro tipo {@code Toast} en la interfaz de usuario.
     *
     * @param message       Mensaje a mostrar.
     * @param shortDuration Booleano que sirve para indicar si el mensaje debe ser de
     *                      corta duraci�n o no. Ingresar falso para un mensaje de larga
     *                      duraci�n o verdadero para uno de duraci�n corta.
     */
    public void showToast(String message, boolean shortDuration) {
        if (toast != null) {
            toast.cancel();
            toast = null;
        }
        toast = Toast.makeText(context, message,
                (shortDuration) ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

}
