package com.voyager.app.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class AppUtils {
    public static Intent openCamara(String url_photo, UIMessages uiMessages) {
        try {
            String fileName = "photo.jpg";
            try {
                File folder = new File(url_photo);
                if (!folder.exists()) {
                    Boolean created = folder.mkdirs();
                    if (!created) {
                        uiMessages.showToast("No se pudo crear la foto, verifique su espacio en disco", true);
                        return null;
                    }
                }
                url_photo += fileName;
                File myPhoto = new File(url_photo);
                if (myPhoto.exists()) {
                    myPhoto.delete();
                }
                Boolean photoCreate = myPhoto.createNewFile();
                if (!photoCreate) {
                    uiMessages.showToast("No se pudo crear la foto, verifique su espacio en disco", true);
                    return null;
                }
                Uri uri = Uri.fromFile(myPhoto);
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                return cameraIntent;

            } catch (IOException ex) {
                Log.e("ERROR ", "Error:" + ex);
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return null;
    }

    public static String getImgBase64(ImageView imvFoto) {
        String imgBase64 = "NO";
        try {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) imvFoto.getDrawable();
            String base64 = parseBase64(bitmapDrawable);
            if (!base64.equals("NO")) {
                imgBase64 = base64;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imgBase64;
    }

    private static String parseBase64(BitmapDrawable bitmapDrawable) {
        if (bitmapDrawable != null) {
            Bitmap bitmap = bitmapDrawable.getBitmap();
            Bitmap bmtChiquito = scaleDown(bitmap, 600, true);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmtChiquito.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            return Base64.encodeToString(byteArray, Base64.DEFAULT);
        }
        return "NO";
    }

    private static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                    boolean filter) {
        float ratio = Math.min((float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }
}
