package com.voyager.app.constant;

public final class Constant {
    //    private  static final String API_URL = "http://10.11.152.149/web_service_voyager";
    private static final String API_URL = "http://52.225.193.222:8081";
    public static final String API_URL_USER = API_URL + "/WSUser.svc";
    public static final String API_URL_PLACE = API_URL + "/WSPlace.svc";
    public static final String API_URL_COMMENT = API_URL + "/WSComment.svc";
    public static final String API_URL_FOLLOWER = API_URL + "/WSFollower.svc";
    public static final String API_URL_PHOTO = API_URL + "/WSPhoto.svc";
    public static final String API_URL_RATING_PLACE = API_URL + "/WSRatingPlace.svc";
    public static final String API_URL_SAVE_PLACE = API_URL + "/WSSavePlace.svc";
    public static final String API_URL_VIEWS_PLACE = API_URL + "/WSViewsPlace.svc";
}